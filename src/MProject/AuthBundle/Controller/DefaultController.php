<?php

namespace MProject\AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MProjectAuthBundle:Default:index.html.twig', array('name' => 'MProject Home'));
    }
}
