<?php

namespace MProject\AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MProject\AuthBundle\Controller\BaseController;

use MProject\AuthBundle\Document\User as UserDocument;
use MProject\AuthBundle\Document\UserRepository;

use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;

/**
 * User controller.
 *
 */
class SecurityController extends BaseController
{
    /** @var UserRepository */
    protected $userRepository;

    /** Show signup form.*/

    public function init()
    {
        $this->request = $this->getRequest();

        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'application/json');

        $this->userRepository = $this->get('user_repository');
    }

    public function showSignupAction()
    {
        $user = new UserDocument();
        $user->setCreateDate(new \DateTime('now'));

        $form = $this->createFormBuilder($user)
                     ->setAction($this->generateUrl('_auth_signup_process'))
                     ->add('firstName', 'text', array('attr' => array('class' => 'required input-xlarge control-group')))
                     ->add('middleName', 'text', array('attr' => array('class' => 'required input-xlarge control-group')))
                     ->add('lastName', 'text', array('attr' => array('class' => 'required input-xlarge control-group')))
                     ->add('email', 'text', array('attr' => array('class' => 'required input-xlarge control-group')))
                     ->add('password', 'password', array('attr' => array('class' => 'required input-xlarge control-group')))
                     ->add('save', 'submit', array('attr' => array('class' => 'required btn btn-success control-group')))
                     ->getForm();

        return $this->render('MProjectAuthBundle:Security:signup.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /** Process Signup action */

    public function signupProcessAction()
    {
        $requestData = $this->request->getContent();
        $path = parse_url($requestData);
        parse_str($path['path'], $data);

        try{
            $userDocument = $this->userRepository->insert($data['form']);

            $this->response->setContent(json_encode(array('result' => $userDocument->toArray())));
            $this->response->setStatusCode(201);

        } catch (AclAlreadyExistsException $e) {

            $this->response->setContent(json_encode(array('result' => $e->getMessage())));
            $this->response->setStatusCode($e->getCode());
        }

        return $this->response;
    }

    /** Show login form.*/

    public function showLoginAction()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('_auth_login_process'))
            ->add('email', 'text', array('attr' => array('class' => 'required input-xlarge control-group')))
            ->add('password', 'password', array('attr' => array('class' => 'required input-xlarge control-group')))
            ->add('save', 'submit', array('attr' => array('class' => 'required btn btn-success control-group')))
            ->getForm();

        return $this->render('MProjectAuthBundle:Security:login.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /** Process Login action */

    public function loginProcessAction()
    {
        $requestData = $this->getRequest()->getContent();
        $path = parse_url($requestData);
        parse_str($path['path'], $data);

        try{
            $userDocument = $this->userRepository->loginProcess($data['form']);

            $session = $this->getRequest()->getSession();
            $session->set('loggedin', 1);
            $session->set('loggedinuser', $userDocument->toSessionArray());

            $this->response->setContent(json_encode(array('result' => $userDocument->toArray())));
            $this->response->setStatusCode(200);

        }  catch (InvalidArgumentException $e) {

            $this->response->setContent(json_encode(array('result' => $e->getMessage())));
            $this->response->setStatusCode($e->getCode());
        }

        if ($this->response->getStatusCode() == 200) {
            echo "login successful!";exit;

        } else {

            return $this->response;
        }
    }

    /** Logout process action */
    public function logoutProcessAction()
    {
        $this->session->clear();

        $this->session->getFlashBag()->set('logout_notice', 'You have been successfully been logged out.');

        $url = $this->generateUrl('mproject_home');
        return $this->redirect($url, 302);
    }
}