<?php

namespace MProject\AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Doctrine\ODM\MongoDB\DocumentManager;
use MProject\AuthBundle\Document\User;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BaseController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var String
     */
    protected $sessionUser;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * Inject the Request object for further use.
     *
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Initializer function to be used by child controller classes.
     */
    public function init(){}

    /**
    * Helper for transitioning, should be removed later
    *
    * @param $name
    *
    * @return \Doctrine\Bundle\MongoDBBundle\ManagerRegistry|object|\stdClass
    */
    public function __get($name)
    {
        switch ($name) {
            case 'dm': return $this->get('doctrine_mongodb');
        }

        if ($this->container->has($name)) {
            return $this->container->get($name);
        }
    }
}