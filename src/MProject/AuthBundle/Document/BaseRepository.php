<?php

namespace MProject\AuthBundle\Document;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\DependencyInjection\Container;
use MProject\AuthBundle\Document\User as UserDocument;

/**
 * BaseRepository
 */
class BaseRepository extends DocumentRepository
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var UserDocument
     */
    protected $sessionUser;

    public function setContainer(Container $container)
    {
        $this->container = $container;

        $mongo = $this->container->get('mongo');
        $this->dm = $mongo->getDm();
    }

    public function setSessionUser($sessionUser)
    {
        $this->sessionUser = $sessionUser;
    }
}
