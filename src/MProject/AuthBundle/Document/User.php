<?php

namespace MProject\AuthBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="users",repositoryClass="MProject\AuthBundle\Document\UserRepository")
 */
class User
{
    /** @ODM\Id */
    protected $id;

    /**
     * @ODM\String
     * @ODM\Index(unique=true, safe=true)
     */
    protected $email;

    /** @ODM\Boolean */
    protected $enabled;

    /** @ODM\String */
    protected $salt;

    /** @ODM\String */
    protected $password;

    /** @ODM\String */
    protected $confirmationToken;

    /** @ODM\Date */
    protected $activationSentAt;

    /** @ODM\String */
    protected $passwordHash;

    /** @ODM\Date */
    protected $passwordRequestedAt;

    /** @ODM\Boolean */
    protected $locked;

    /** @ODM\String */
    protected $firstName;

    /** @ODM\String */
    protected $middleName;

    /** @ODM\String */
    protected $lastName;

    /** @ODM\String */
    protected $avatar;

    /** @ODM\String */
    protected $authToken;

    /** @ODM\Date */
    protected $createDate;

    /** @ODM\Date */
    protected $updateDate;

    public function toArray()
    {
        $data = array(
            'id'                    => $this->getId(),
            'email'                 => $this->getEmail(),
            'first_name'            => $this->getFirstName(),
            'middle_name'           => $this->getMiddleName(),
            'last_name'             => $this->getLastName(),
            'avatar'                => $this->getAvatar(),
            'enabled'               => $this->getEnabled(),
            'salt'                  => $this->getSalt(),
            'password'              => $this->getPassword(),
            'confirmation_token'    => $this->getConfirmationToken(),
            'password_hash'         => $this->getPasswordHash(),
            'password_requested_at' => $this->getPasswordRequestedAt(),
            'activation_sent_at'    => $this->getActivationSentAt(),
            'locked'                => $this->getLocked(),
            'auth_token'            => $this->getAuthToken(),
            'create_date'           => $this->getCreateDate(),
            'update_date'           => $this->getUpdateDate()
        );

        return $data;
    }

    public function toSessionArray()
    {
        $data = array(
            'id'          => $this->getId(),
            'email'       => $this->getEmail(),
            'first_name'  => $this->getFirstName(),
            'middle_name' => $this->getMiddleName(),
            'last_name'   => $this->getLastName(),
            'auth_token'  => $this->getAuthToken(),
        );

        return $data;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setActivationSentAt($activationSentAt)
    {
        $this->activationSentAt = $activationSentAt;
    }

    public function getActivationSentAt()
    {
        return $this->activationSentAt;
    }

    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
    }

    public function getAuthToken()
    {
        return $this->authToken;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    public function getLocked()
    {
        return $this->locked;
    }

    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    public function getMiddleName()
    {
        return $this->middleName;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    public function setPasswordRequestedAt($passwordRequestedAt)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
    }

    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function getUpdateDate()
    {
        return $this->updateDate;
    }

}
