<?php

namespace MProject\AuthBundle\Document;

use Doctrine\ODM\MongoDB\Mapping;
use MProject\AuthBundle\Document\User as UserDocument;

class UserMapper
{
    public function map(UserDocument $userDocument, $data)
    {
        if (!empty($data['email'])) {
            $userDocument->setEmail($data['email']);
            $userDocument->setSalt($data['email']);
        }

        if (!empty($data['firstName'])) {
            $userDocument->setFirstName($data['firstName']);
        }

        if (!empty($data['middleName'])) {
            $userDocument->setMiddleName($data['middleName']);
        }

        if (!empty($data['lastName'])) {
            $userDocument->setLastName($data['lastName']);
        }

        if (!empty($data['avatar'])) {
            $userDocument->setAvatar($data['avatar']);
        }

        if (!empty($data['enabled'])) {
            $userDocument->setEnabled((bool)$data['enabled']);
        }

        if (!empty($data['password'])) {
            $userDocument->setPassword($data['password']);
        }

        if (!empty($data['confirmation_token'])) {
            $userDocument->setConfirmationToken($data['confirmation_token']);
        }

        if (!empty($data['password_hash'])) {
            $userDocument->setPasswordHash($data['password_hash']);
        }

        if (!empty($data['locked'])) {
            $userDocument->setLocked((bool)$data['locked']);
        }

        if (!empty($data['auth_token'])) {
            $userDocument->setAuthToken($data['auth_token']);
        }

        if (!empty($data['create_date'])) {
            $userDocument->setCreateDate(new \DateTime($data['create_date']));
        }

        if (!empty($data['password_requested_at'])) {
            $userDocument->setPasswordRequestedAt(new \DateTime($data['password_requested_at']));
        }

        if (!empty($data['activation_sent_at'])) {
            $userDocument->setActivationSentAt(new \DateTime($data['activation_sent_at']));
        }

        $userDocument->setUpdateDate(new \DateTime());

        return $userDocument;
    }
}