<?php

namespace MProject\AuthBundle\Document;

use MProject\AuthBundle\Document\BaseRepository;
use MProject\AuthBundle\Document\User as UserDocument;
use MProject\AuthBundle\Document\UserMapper as UserMapper;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;

/**
 * UserRepository
 */
class UserRepository extends BaseRepository
{

    public function insert($data)
    {
        $entity = new UserDocument();
        $mapper = new UserMapper();

        if ($this->isExists($data['email'])) {
            throw new AclAlreadyExistsException('An user with email: ' . $data['email'] . ' already exists!', 401);
        }

        $mapped = $mapper->map($entity, $data);

        // settinsdefault values generation at the back
        $mapped->setEnabled(true);
        $mapped->setLocked(false);
        $mapped->setPassword(sha1($mapped->getPassword() . $mapped->getSalt()));
        $mapped->setAuthToken(sha1($mapped->getEmail() . $mapped->getSalt()));

        $mapped->setConfirmationToken(sha1($mapped->getEmail() . $mapped->getSalt() . time()));

        $currentDate = Date('Y-m-d H:i:s');
        $addInterval = $this->getDateAfterInterval($currentDate, 1);
        $dateAfterInterval = new \DateTime($addInterval);
        $mapped->setActivationSentAt($dateAfterInterval);

        $mapped->setCreateDate(new \DateTime());

        $this->dm->persist($mapped);
        $this->dm->flush();

        return $mapped;
    }


    public function isExists($email)
    {
        $entity = $this->findOneBy(array('email' => $email));

        if ($entity){
            return true;
        }

        return false;
    }

    public function getDateAfterInterval($now, $interval)
    {
        return Date('Y-m-d H:i:s', (strtotime(date("Y-m-d H:i:s", strtotime($now)) . " +" . $interval . " days")));
    }

    public function getDocument($id)
    {
        $entity = $this->find($id);

        if (!$entity) {
            return false;
        }

        return $entity;
    }

    public function getAll()
    {
        $users = $this->findAll();

        $allUsers = array();
        foreach($users as $user) {
            $allUsers[] = $user->toArray();
        }

        return $allUsers;
    }

    public function update($id, $data)
    {
        $entity = $this->find($id);

        if (!$entity) {
            throw new InvalidArgumentException('No such user found', 402);
        }

        $mapper = new UserMapper();

        $mapped = $mapper->map($entity, $data);

        $this->dm->persist($mapped);
        $this->dm->flush();

        return $mapped;
    }

    public function delete($id)
    {
        $userDocument = $this->find($id);

        if (!$userDocument) {
            throw new InvalidArgumentException('No such user found', 402);
        }

        $this->dm->remove($userDocument);
        $this->dm->flush();

        return true;
    }

    public function loginProcess($data)
    {
        $userDocument = $this->findOneBy(array('email' => $data['email']));

        if (!$userDocument) {
            throw new InvalidArgumentException('Invalid email address or password given', 402);
        }

        $dbPassword = $userDocument->getPassword();
        $inputPassword = sha1($data['password'] . $data['email']);

        if ($dbPassword != $inputPassword) {
            throw new InvalidArgumentException('Invalid email address or password given', 402);
        }

       /* if (!$userDocument->getEnabled()) {
            throw new InvalidArgumentException('You have not activated your account yet', 402);
        }

        if ($userDocument->getLocked()) {
            throw new InvalidArgumentException('Your account has been blocked by one of the admins', 402);
        }*/

        return $userDocument;
    }
}
